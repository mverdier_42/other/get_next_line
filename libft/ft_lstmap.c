/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 19:49:52 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/05 20:09:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new;
	t_list *tmp;

	tmp = lst;
	if (lst)
	{
		new = (*f)(lst);
		tmp = new;
		lst = lst->next;
	}
	while (lst)
	{
		new = (*f)(lst);
		ft_lstadd_end(&tmp, new);
		lst = lst->next;
	}
	return (tmp);
}
