/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:55 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 15:57:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static int	ft_cmp(const char *big, const char *little, size_t *tab)
{
	while ((tab[VAR_I] < tab[VAR_TMP] && big[tab[VAR_I]]) && little[tab[VAR_J]])
	{
		if (big[tab[VAR_I]] == little[tab[VAR_J]])
		{
			tab[VAR_I]++;
			tab[VAR_J]++;
		}
		else
			break ;
		if (!big[tab[VAR_I]] && little[tab[VAR_J]])
			return (1);
	}
	return (0);
}

static void	ft_f_ocur(const char *big, const char *little, size_t *tab)
{
	while (big[tab[VAR_I]] && big[tab[VAR_I]] != little[0])
		tab[VAR_I]++;
}

static char	*ft_search(const char *big, const char *little, size_t len)
{
	size_t tab[4];

	tab[VAR_I] = 0;
	tab[VAR_TMP] = 0;
	while (big[tab[VAR_I]])
	{
		tab[VAR_TMP] = len + tab[VAR_I];
		tab[VAR_J] = 0;
		ft_f_ocur(big, little, tab);
		if (!big[tab[VAR_I]])
			return (NULL);
		tab[VAR_K] = tab[VAR_I];
		if ((ft_cmp(big, little, tab)) == 1)
			return (NULL);
		if (!big[tab[VAR_I]] || !little[tab[VAR_J]])
			return ((char *)big + tab[VAR_K]);
		tab[VAR_I] = tab[VAR_K] + 1;
	}
	return (NULL);
}

char		*ft_strnstr(const char *big, const char *little, size_t len)
{
	if (!little[0])
		return ((char *)big);
	return (ft_search(big, little, len));
}
